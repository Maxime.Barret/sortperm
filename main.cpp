#include <iostream>
#include <string>
#include <cassert>
#include <exception>
#include <iterator>
#include "SortPerm.hpp"
#include "Types.hpp"

using namespace TYPES_Iterator;


static int tableOK[] = {2, 6, 4, 0, 10};
static SortPerm<int> tableSort{tableOK, tableOK + 5};

static int table2[] = {2, 56, 77, 50, 10};

template<typename T>
std::string toString(const T* tab, unsigned size)
{
    std::stringstream ss;
    std::for_each(tab, tab + size, [&](const T& elem) {ss << elem << '|';});
    std::string s {ss.str()};
    return s.substr(0, s.size() - 1);
}

void testTableValue()
{
    const std::string str1 { tableSort.printValue() };
    const std::string str2 {"0|2|4|6|10"};

    if (str1 != str2)
    {
        std::stringstream ss; 
        ss << "TestTable Fail : " << str1 << " != " << str2;
        throw std::out_of_range(ss.str().c_str());
    }
}

void testTableIndex()
{
    const std::string str1 { tableSort.printIndex() };
    const std::string str2 {"3|0|2|1|4"};

    if (str1 != str2)
    {
        std::stringstream ss; 
        ss << "TestTable Fail : " << str1 << " != " << str2;
        throw std::out_of_range(ss.str().c_str());
    }
}

void testTablePermute()
{
    int tab[] = {10, 5, 3, 2, 9};
    int permutedTab[5];
    tableSort.permute(tab, tab + 5, permutedTab);
    std::string str1 { "5|2|3|10|9" };
    std::string str2 = toString(permutedTab, 5);
    if (str1 != str2)
    {
        std::stringstream ss; 
        ss << "TestPermute Fail : " << str1 << " != " << str2;
        throw std::out_of_range(ss.str().c_str());
    }
}

int main() 
{
    try{
        std::cout << "Test du tri des valeurs" << std::endl;
        testTableValue();
    }
    catch(const std::exception& e){
        std::cerr << e.what() << '\n';
    }
    
    try{
        std::cout << "Test des indices" << std::endl;
        testTableIndex();
    }
    catch(const std::exception& e){
        std::cerr << e.what() << '\n';
    }

    try{
        std::cout << "Test de permutation" << std::endl;
        testTablePermute();
    }
    catch(const std::exception& e){
        std::cerr << e.what() << '\n';
    }

    return 0;
}

