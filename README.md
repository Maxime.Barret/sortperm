**Classe C++ : SortPerm**

Classe C++ permettant de permuter les éléments d'un conteneur en fonction des indices d'un conteneur trié.


Exemple : 

    Conteneur A       : [ 1 6 9 10 5 ]
    Indices   I       : [ 0 1 2 3 4 ]
    Triage    At      : [ 1 5 6 9 10 ]
    Indices de A, It  : [ 0 4 1 2 3 ]

    Conteneur B       : [ 5 6 0 9 2 ]
    Permutation de B  : [ 5 2 6 0 9 ]

    -> At = A(I)

Implémtentation de la méthode MATLAB : [l2, perm] = sort(l1);
