#ifndef SortPerm_hpp
#define SortPerm_hpp

#include <algorithm>
#include <set>
#include <tuple>
#include <iterator>
#include <exception>
#include <string>
#include <sstream>

template<class T>
class SortPerm 
{
    typedef std::tuple<T, unsigned int> T_tuple;
    typedef std::set<T_tuple> T_Set;

public:
    template<class InputIterator>
    SortPerm(InputIterator first, InputIterator last)
    {
        unsigned int n = 0;
        std::for_each(first, last, [&] (const T& elem) {set_.insert(std::make_tuple(elem, n++));} );
    }

    template<class InputIterator>
    void permute(InputIterator first, InputIterator last, InputIterator out)
    {
        auto elemSet = set_.begin();
        auto elem = first;
        while (elemSet != set_.end())
        {
            unsigned int idx = std::get<1>(*elemSet);
            auto beg = out;
            std::advance(beg, idx);
            *beg = *elem;
            ++elem;
            ++elemSet;
        }
    }

    std::string printValue() const
    {
        std::stringstream ss;
        std::for_each(set_.begin(), set_.end(), [&](const T_tuple& tuple) {ss << std::get<0>(tuple) << '|';});
        std::string s = ss.str();
        return s.substr(0, s.size() - 1);
    }

    std::string printIndex() const
    {
        std::stringstream ss;
        std::for_each(set_.begin(), set_.end(), [&](const T_tuple& tuple) {ss << std::get<1>(tuple) << '|';});
        std::string s = ss.str();
        return s.substr(0, s.size() - 1);
    }
private:
    T_Set set_;
};

#endif